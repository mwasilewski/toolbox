FROM fedora:latest
MAINTAINER michal.wasilewski.email@gmail.com

# RUN dnf -y update

RUN dnf -y install \
augeas \
atop \
bind-utils \
binutils \
edac-utils \
fping \
gcc \
git \
ipmitool \
iproute \
iputils \
minicom \
net-tools \
nload \
nmap \
parallel \
procps-ng \
pssh \
psutils \
python2 \
python-devel \
redhat-rpm-config \
rsnapshot \
strace \
sysstat \
telnet \
tmux


RUN dnf clean all

CMD tail -f /dev/null
